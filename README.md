# YoogoShare Install Tool 

This repository contains the source code behind the 'Installer App'.

##This has the following main functionalities.
* Provide access to VIEW diagnostic information
* Provide access to EDIT and CREATE Vehicles
* Provide access to SEND SMS commands from a set list of available commands(**Custom SMS not available**) 

* This tool does NOT facilitate adding new KeazKits. 

 The level of access required prior to being able to access this platform is 'installer'. Without the 'installer' permission in your set of permissions, you aren't able to access this platform.

## Repository Information
This tool is built off of the Angular JS Admin Starter Theme
The build tool used is GULP
