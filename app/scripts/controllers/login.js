'use strict';
$ = jQuery;
/**
 * @ngdoc function
 * @name yapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of yapp
 */
angular.module('yapp')
  .controller('LoginCtrl', function ($scope, keazAPI, Auth, $stateParams, $rootScope, $location,$state) {
    Auth.destroyAll();
    $scope.submit = function () {
      var form = $(frmLogin).serializeObject();
      form['timezone_offset'] = new Date().getTimezoneOffset() * 60;
      keazAPI.login(form).success(function (data) {
        Auth.createSession({
          'token': data['token'],
          'email': form['email']
        });
        if ($.cookie("token") != '') {
          //console.log("token not empty");
          $location.path('/company');
        } else {
          Auth.destroyAll();
        }
      }).error(function () {
        Auth.destroyAll();
      });
    }
  });
