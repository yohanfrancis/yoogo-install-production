'use strict';
$ = jQuery;
var marker = null;
var map = null;
/**
 * @ngdoc function
 * @name yapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of yapp
 */
angular.module('yapp')
  .controller('DashboardCtrl', function ($scope, keazAPI, Auth, $stateParams, $rootScope, $location, $state) {
    $scope.$state = $state;
  })
  .controller('CompanyCtrl', function ($scope, keazAPI, Auth, $stateParams, $rootScope, $location, $state) {
    $rootScope.message = '';
    $scope.$state = $state;
    $scope.chooseCompany = function (company_id) {
      $rootScope.choose_company = company_id;
      $.cookie("company", $rootScope.user_data.companies[company_id]['slug']);
      $rootScope.user_data.company = $rootScope.user_data.companies[company_id];
      $location.path('/select');
    }
  })
  .controller('SelectCtrl', function ($scope, keazAPI, Auth, $stateParams, $rootScope, $location, $state) {
    $rootScope.message = '';
    $scope.$state = $state;
    $rootScope.selectWhatToDo = [
      {"id": 0, "name": "Vehicle List", "path": "/vehicles"},
      {"id": 1, "name": "New Vehicle", "path": "/vehicle"},
      {"id": 2, "name": "Assign Kit", "path": "/assign_kit"}
    ];
    $scope.selectWork = function (work_id) {
      //console.log(work_id);
      //console.log($rootScope.selectWhatToDo[work_id].path);
      $location.path($rootScope.selectWhatToDo[work_id].path);
    }
  })
  .controller('DetailVehicleCtrl', function ($scope, keazAPI, Auth, $stateParams, $rootScope, $location, $state) {
    $rootScope.message = '';
    $scope.vehicle = {};
    $scope.$state = $state;
    $scope.submitVehicle = function () {
      var form = $(frmVehicle).serializeObject();
      if (typeof form['feature_ids'] == 'string') {
        form['feature_ids'] = [form['feature_ids']];
      }
      if (!form['feature_ids']) {
        form['feature_ids'] = []
      }
      if (typeof form['cost-centre_ids'] == 'string') {
        form['cost-centre_ids'] = [form['cost-centre_ids']];
      }
      if (!form['cost-centre_ids']) {
        form['cost-centre_ids'] = []
      }
      if (!$stateParams.id) {
        keazAPI.post('vehicle', form).success(function (response) {
          $location.path('/test_vehicle/' + response.id);
        }).error(function () {
          $rootScope.show_msg(response.description, false);
        });
      } else {
        keazAPI.put('vehicle/' + $stateParams.id, form).success(function (response) {
          $location.path('/test_vehicle/' + response.id);
        }).error(function (response) {
          $rootScope.show_msg(response.description, false);
        });
      }

    };

    $scope.seats = [
      {name: 'One', id: '1'},
      {name: 'Two', id: '2'},
      {name: 'Three', id: '3'},
      {name: 'Four', id: '4'},
      {name: 'Five', id: '5'},
      {name: 'Six', id: '6'},
      {name: 'Seven', id: '7'},
      {name: 'Eight', id: '8'},
      {name: 'Nine', id: '9'},
      {name: 'Ten', id: '10'}
    ];
    $scope.luggages = [
      {name: 'Motobike', id: '1'},
      {name: 'Two', id: '2'},
      {name: 'Four', id: '4'},
      {name: 'Five', id: '5'},
      {name: '6+ ', id: '6'}
    ];
    $scope.vehicle_doors = [
      {name: 'One', id: '1'},
      {name: 'Two', id: '2'},
      {name: 'Three', id: '3'},
      {name: 'Four', id: '4'},
      {name: 'Five', id: '5'},
      {name: '6+', id: '6'}
    ];

    $scope.fuel_type = [
      {name: 'Unleaded', id: 'Unleaded'},
      {name: 'Premium', id: 'Premium'},
      {name: 'E10', id: 'E10'},
      {name: 'Diesel', id: 'Diesel'},
      {name: 'LPG', id: 'LPG'}
    ];

    $scope.colors = [
      {name: 'None', id: 'none'},
      {name: 'White', id: '#f6f6f6'},
      {name: 'Yellow', id: '#f6c82d'},
      {name: 'Orange', id: '#ef6b23'},
      {name: 'Red', id: '#cc352e'},
      {name: 'Maroon', id: '#8d1a1f'},
      {name: 'Purple', id: '#550673'},
      {name: 'Blue', id: '#0062ad'},
      {name: 'Green', id: '#007b67'},
      {name: 'Brown', id: '#935421'},
      {name: 'Black', id: '#000000'},
      {name: 'Silver', id: '#939aa2'},
      {name: 'Silver', id: '#bfb65b'}
    ];

    $scope.transmission = [
      {name: 'Automatic', id: 'A'},
      {name: 'Manual', id: 'M'}
    ];

    $scope.booleans = [
      {name: 'Yes', id: '1'},
      {name: 'No', id: '0'}
    ];


    if ($stateParams.id) {
      keazAPI.get('vehicle/fuels?orderby=name', true).success(function (data) {
        console.log("success models", data);
        $scope.vehicleFuels = data;
        keazAPI.get('vehicle/' + $stateParams.id).success(function (response) {
          $scope.vehicle = response;
          keazAPI.get('cost-centres').success(function (data) {
            //console.log("cost-centres", data);
            $scope.checkCentres = {};
            $scope.listCostCentres = data;
            $scope.center_checkeds = [];
            for (var i = 0; i < $scope.vehicle['cost-centre'].length; i++) {
              $scope.center_checkeds.push(parseInt($scope.vehicle['cost-centre'][i]['id']));
            }
            for (var i = 0; i < $scope.listCostCentres.length; i++) {
              var id = $scope.listCostCentres[i]['id'];
              if ($.inArray(parseInt(id), $scope.center_checkeds) > -1) {
                $scope.checkCentres[id] = true;
              } else {
                $scope.checkCentres[id] = false;
              }
            }
            setTimeout(function () {
              $scope.$apply();
              $scope.$applyAsync();
            }, 3000);

          });

          keazAPI.get('branches').success(function (data) {
            //console.log(data);
            $scope.listBranch = data;

          });


          keazAPI.get('vehicle/features').success(function (data) {
            $scope.checkFeatures = {};
            //console.log("success features", data);
            $scope.vehicleFeatures = data;
            $scope.feature_checkeds = [];
            for (var i = 0; i < $scope.vehicle.features.length; i++) {
              $scope.feature_checkeds.push(parseInt($scope.vehicle.features[i]['id']));
            }

            for (var i = 0; i < $scope.vehicleFeatures.length; i++) {
              var id = $scope.vehicleFeatures[i]['id'];
              if ($.inArray(parseInt(id), $scope.feature_checkeds) > -1) {
                $scope.checkFeatures[id] = true;
              } else {
                $scope.checkFeatures[id] = false;
              }
            }


          });
          keazAPI.get('vehicle/models?orderby=name', true).success(function (data) {
            //console.log("success models", data);
            $scope.vehicleModels = data;
            $("#vehicle_model").combobox();
            if ($scope.vehicle.model) {
              var result = $.grep(data, function(e){ return e.id == $scope.vehicle.model.id; });
              if (result.length > 0) {
                $('#warp_vehicle_model .custom-combobox-input').val($scope.vehicle.model.name);
              }
            }
          });


          keazAPI.get('vehicle/kitavalibles?vehicle_id='+$stateParams.id,true).success(function (data) {
            //console.log("success kits", data);
            $scope.vehicleKits = data;
            $("#vehicle_kit").combobox();
            if ($scope.vehicle.kit) {
              var result = $.grep(data, function(e){ return e.id == $scope.vehicle.kit.id; });
              if (result.length > 0) {
                $('#warp_vehicle_kit .custom-combobox-input').val($scope.vehicle.kit.serial);
              }
            }
          });


        }).error(function () {
          $rootScope.show_msg('Get vehicle error', false);
        });
      });


    } else {
      $scope.vehicle = {};
      keazAPI.get('cost-centres').success(function (data) {
        //console.log("cost-centres", data);
        $scope.checkCentres = {};
        $scope.listCostCentres = data;
        $scope.center_checkeds = [];
      });

      keazAPI.get('branches').success(function (data) {
        //console.log(data);
        $scope.listBranch = data;

      });


      keazAPI.get('vehicle/features').success(function (data) {
        $scope.checkFeatures = {};
        //console.log("success features", data);
        $scope.vehicleFeatures = data;
        $scope.feature_checkeds = [];
      });
      keazAPI.get('vehicle/models?orderby=name', true).success(function (data) {
        //console.log("success models", data);
        $scope.vehicleModels = data;
        $("#vehicle_model").combobox();
      });
      keazAPI.get('vehicle/fuels?orderby=name', true).success(function (data) {
        console.log("success models", data);
        $scope.vehicleFuels = data;
        //$("#vehicle_fuel").combobox();
      });
      keazAPI.get('vehicle/kitavalibles').success(function (data) {
        //console.log("success kits", data);
        $scope.vehicleKits = data;
        $("#vehicle_kit").combobox();
      });

      // $scope.$apply();
      $scope.$applyAsync();
    }
  })
  .controller('AssignKitCtrl', function ($scope, keazAPI, Auth, $stateParams, $rootScope, $location, $state) {
    $scope.form = {
      'rfid': ''
    };
    $scope.buttonStartListen = 'start';
    keazAPI.get('vehicle/kits').success(function (data) {
      $scope.vehicleKits = data;
      $("#vehicle_kit").combobox({
        select: function (event, ui) {
          $('#kit_serial').val($(ui.item).attr('serial'));
        }
      });
    });
    keazAPI.get('users').success(function (data) {
      $scope.users = data;
      $("#user").combobox({
        select: function (event, ui) {
          //$('#rfid').val(ui.item.option.attributes.rfid.value);
        }
      });
    });
    $scope.clearForm = function () {
      $scope.buttonStartListen = 'start';
      $scope.form = $(frmAssignKit).serializeObject();
      $scope.form.kit_serial = '';
    };
    $scope.startListen = function () {
      if ($scope.buttonStartListen != 'stop') {
        $scope.buttonStartListen = 'stop';
        $('#warp_vehicle_kit .custom-combobox-input').attr("disabled", "disabled");
      } else {
        $scope.buttonStartListen = 'start';
        $('#warp_vehicle_kit .custom-combobox-input').removeAttr("disabled");
      }
      $scope.form = $(frmAssignKit).serializeObject();
      function call_forever() {
        if ($scope.buttonStartListen == 'stop' && $scope.form.kit_serial != '') {
          keazAPI.get('udp/'+$scope.form['kit_serial']+'/4').success(function (response) {
            $rootScope.load = false;
            if (!response['Rfid']) {
              response['Rfid'] = '';
            }
            if (response['Rfid']) {
              var new_rfid = response['Rfid'].toString();
              $scope.$watch(function () {
                $scope.form.rfid = new_rfid.toString();
              });
            }
            setTimeout(function () {
              call_forever();
            }, 2000);
          }).error(function () {
            $rootScope.show_msg('Socket is error', false);
          });
        }
      }

      call_forever();
    };
    $scope.submitAssignKit = function () {
      $scope.form = $(frmAssignKit).serializeObject();
      keazAPI.put('user/' + $scope.form.user_id, {
        'rfid': $scope.form.rfid
      }).success(function (response) {
        $rootScope.show_msg('RFID has been updated', true);
      }).error(function (response) {
        $rootScope.show_msg(response.description, false);
      });
    }
  })
  .controller('ListVehicleCtrl', function ($scope, keazAPI, Auth, $stateParams, $rootScope, $location, $state) {
    $rootScope.message = '';
    $scope.vehicle = {};
    $scope.rfid = {};
    $scope.$state = $state;
    //console.log($rootScope.user_data);
    // TODO: get list vehicle
    keazAPI.get('vehicles').success(function (data) {
      $scope.listVehicle = data;
    });
    $scope.editVehicle = function (vehicle_id) {
      $location.path("/vehicle/" + vehicle_id);
    };
    $scope.testVehicle = function (vehicle_id) {
      $location.path("/test_vehicle/" + vehicle_id);
    }
  })
  .controller('TestVehicleCtrl', function ($scope, keazAPI, Auth, $stateParams, $rootScope, $location, $sce, $state) {
    $rootScope.message = '';
    $scope.$state = $state;
    $scope.editVehicle = function (vehicle_id) {
      $location.path("/vehicle/" + vehicle_id);
    };
    keazAPI.get('vehicle/' + $stateParams.id).success(function (response) {
      $scope.vehicle = response;

      $scope.address_on_map = '';
      if ($scope.vehicle.lat && $scope.vehicle.lng) {
        var url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + $scope.vehicle.lat + "," + $scope.vehicle.lng;
        $.get(url, function (data) {
          if (data.status == 'OK') {
            $scope.address_on_map = data.results[0].formatted_address;
          }
        });
      }

      if ($scope.vehicle.kit && $scope.vehicle.kit.serial != undefined && $scope.vehicle.kit.serial != null && $scope.vehicle.kit.serial != '' && $scope.vehicle.kit.serial != 'None') {
        keazAPI.get('udp/'+$scope.vehicle.kit.serial+'/4').success(function (response2) {
          $scope.rfid = response2;
          if ($scope.rfid['Door']) {
            $scope.rfid['result'] = 'Door: ' + (($scope.rfid['Door'].toString() == '52' || $scope.rfid['Door'].toString() == '12') ? 'Lock' : 'Unlock').toString() + ', RFID: ' + $scope.rfid['Rfid'].toString();
            $scope.vehicle.lat = parseFloat($scope.rfid.Latitude);
            $scope.vehicle.lng = parseFloat($scope.rfid.Longitude);
          }
          if ($scope.vehicle.lng != '' && $scope.vehicle.lng != '') {
            $scope.vehicle.lat = parseFloat($scope.vehicle.lat);
            $scope.vehicle.lng = parseFloat($scope.vehicle.lng);
            map = new google.maps.Map(document.getElementById('map'), {
              center: {lat: $scope.vehicle.lat, lng: $scope.vehicle.lng},
              zoom: 10
            });
            marker = new google.maps.Marker({
              position: {lat: $scope.vehicle.lat, lng: $scope.vehicle.lng},
              map: map,
              title: $scope.vehicle.name
            });

            var content = '<strong style="color:#000">' + $scope.vehicle.name + ' </strong><p style="color:#000">';
            if ($scope.address_on_map != '') {
              content += '<p style="color:#000">' + $scope.address_on_map + '</p>';
            }
            google.maps.event.addListener(marker, "click", function (e) {
              var iw = new google.maps.InfoWindow({
                content: content,
              }).open(map, this);
            });
          }
          if ($scope.$state.current.name == 'test_vehicle') {
            if ($scope.rfid['Door']) {
              call_forever();
            }
          }
        }).error(function () {
          $rootScope.show_msg('Get socket error', false);
        });
      }
    }).error(function () {
      $rootScope.show_msg('Get vehicle error', false);
    });
    function call_forever() {
      keazAPI.get('udp/' + $scope.vehicle.kit.serial + '/4').success(function (response) {
        $rootScope.load = false;
        $scope.$watch(function () {
          $scope.rfid = response;
          $scope.rfid['result'] = 'Door: ' + (($scope.rfid['Door'].toString() == '52' || $scope.rfid['Door'].toString() == '12') ? 'Lock' : 'Unlock') + ', RFID: ' + $scope.rfid['Rfid'].toString();
          if (response['EventTimeTag'] != $scope.rfid['EventTimeTag']) {
            $scope.vehicle.lat = parseFloat($scope.rfid.Latitude);
            $scope.vehicle.lng = parseFloat($scope.rfid.Longitude);
            marker = new google.maps.Marker({
              position: {lat: $scope.vehicle.lat, lng: $scope.vehicle.lng},
              map: map,
              title: $scope.vehicle.name
            });
          }
        });
        setTimeout(function () {
          call_forever();
        }, 3000);
      }).error(function () {
        $rootScope.show_msg('Get socket error', false);
        return null;
      });
    }

    $scope.kit_enable = function (kit_id) {
      keazAPI.get('vehicle/' + kit_id + '/command/enable', true).success(function (response) {
        $rootScope.show_msg(response.result, true);
      }).error(function () {
        $rootScope.show_msg('There was an error while sending a SMS. Please check it is a valid mobile number.', false);
      });
    };
    $scope.kit_disable = function (kit_id) {
      keazAPI.get('vehicle/' + kit_id + '/command/disable', true).success(function (response) {
        $rootScope.show_msg(response.result, true);
      }).error(function () {
        $rootScope.show_msg('There was an error while sending a SMS. Please check it is a valid mobile number.', false);
      });
    };
    $scope.kit_lock = function (kit_id) {
      keazAPI.get('vehicle/' + kit_id + '/command/lock', true).success(function (response) {
        $rootScope.show_msg(response.result, true);
      }).error(function () {
        $rootScope.show_msg('There was an error while sending a SMS. Please check it is a valid mobile number.', false);
      });
    };
    $scope.kit_unlock = function (kit_id) {
      keazAPI.get('vehicle/' + kit_id + '/command/unlock', true).success(function (response) {
        $rootScope.show_msg(response.result, true);
      }).error(function () {
        $rootScope.show_msg('There was an error while sending a SMS. Please check it is a valid mobile number.', false);
      });
    };

    $scope.kit_device_program = function (kit_id) {
      keazAPI.get('vehicle/' + kit_id + '/command/device_program', true).success(function (response) {
        $rootScope.show_msg(response.result, true);
      }).error(function () {
        $rootScope.show_msg('There was an error while sending a SMS. Please check it is a valid mobile number.', false);
      });
    };
    $scope.kit_device_restart = function (kit_id) {
      keazAPI.get('vehicle/' + kit_id + '/command/device_restart', true).success(function (response) {
        $rootScope.show_msg(response.result, true);
      }).error(function () {
        $rootScope.show_msg('There was an error while sending a SMS. Please check it is a valid mobile number.', false);
      });
    };
    $scope.kit_available = function (kit_id) {
      keazAPI.get('vehicle/' + kit_id + '/command/available', true).success(function (response) {
        $rootScope.show_msg(response.result, true);
      }).error(function () {
        $rootScope.show_msg('There was an error while sending a SMS. Please check it is a valid mobile number.', false);
      });
    };
    $scope.kit_unavailable = function (kit_id) {
      keazAPI.get('vehicle/' + kit_id + '/command/unavailable', true).success(function (response) {
        $rootScope.show_msg(response.result, true);
      }).error(function () {
        $rootScope.show_msg('There was an error while sending a SMS. Please check it is a valid mobile number.', false);
      });
    };
    $scope.kit_location = function (kit_id) {
      keazAPI.get('vehicle/' + kit_id + '/command/location', true).success(function (response) {
        $rootScope.show_msg(response.result, true);
      }).error(function () {
        $rootScope.show_msg('There was an error while sending a SMS. Please check it is a valid mobile number.', false);
      });
    };
    $scope.kit_device_update = function (kit_id) {
      keazAPI.get('vehicle/' + kit_id + '/command/device_update', true).success(function (response) {
        $rootScope.show_msg(response.result, true);
      }).error(function () {
        $rootScope.show_msg('There was an error while sending a SMS. Please check it is a valid mobile number.', false);
      });
    };
    $scope.kit_program_rfid = function (kit_id) {
      keazAPI.post('vehicle/' + kit_id + '/command/custom',{
        'message':"!R1,2178,0,<EH><S0><D1><=*><T1>"
      }, true).success(function (response) {
        $rootScope.show_msg(response.result, true);
      }).error(function () {
        $rootScope.show_msg('There was an error while sending a SMS. Please check it is a valid mobile number.', false);
      });
    };
    $scope.kit_clear_rfid = function (kit_id) {
      keazAPI.post('vehicle/' + kit_id + '/command/custom', {
        'message':"!R1,2178,0,"
      },true).success(function (response) {
        $rootScope.show_msg(response.result, true);
      }).error(function () {
        $rootScope.show_msg('There was an error while sending a SMS. Please check it is a valid mobile number.', false);
      });
    };
    $scope.kit_zoom = function (lat, lng) {
      //console.log(map);
      map.setCenter(new google.maps.LatLng(lat, lng));
      map.setZoom(18);
    };
  });
