'use strict';
$ = jQuery;
angular.module('yapp').run(function ($rootScope, $location, Auth, keazAPI, $timeout, $anchorScroll) {
  $rootScope.isKeaz = function () {
    if (_.contains($rootScope.uperms, 'keaz')) {
      return true;
    } else {
      return false;
    }
  };
  $rootScope.isAccess = function (_perm) {

    if (!$rootScope.uperms) {
      return false;
    }

    if (_.contains($rootScope.uperms, 'admin')) {
      return true;
    }

    if (_.contains($rootScope.uperms, _perm)) {
      return true;
    }

    return false;
  };
  $rootScope.isAccessVehicleKit = function (_perm) {
    if ($rootScope.isAccess(_perm)) {
      if ($rootScope.p != null && $rootScope.p.company.enable_telematic != 'False' && $rootScope.p.company.enable_telematic != false) {
        return true;
      }
    }
    return false;
  };
  $rootScope.isAccessReport = function (_perm) {
    if ($rootScope.isAccess(_perm)) {
      if ($rootScope.p != null && $rootScope.p.company.enable_report != 'False' && $rootScope.p.company.enable_report != false) {
        return true;
      }
    }
    return false;
  };
  $rootScope.show_msg = function (message,success) {
    $rootScope.message = message;
    if (!success) {
      $('#Message').addClass('error');
    } else {
      $('#Message').removeClass('error');
    }
    $timeout(function(){
      $rootScope.message = '';
    },3000);
  };
  $rootScope.menuOpen = false;
  $rootScope.load = false;
});


