'use strict';
$ = jQuery;
/**
 * @ngdoc overview
 * @name yapp
 * @description
 * # yapp
 *
 * Main module of the application.
 */
angular
  .module('yapp', [
    'service',
    'ui.router',
    'snap',
    'ngAnimate'
  ])
  .config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
    $urlRouterProvider.otherwise('/login');
    $stateProvider
      .state("auth", {
        abstract: true,
        url: "",
        templateUrl: 'layout/base.html',
        resolve: {
          User: ["keazAPI", "$rootScope", "$location", "$q", "Auth", "$timeout", function (keazAPI, $rootScope, $location, $q, Auth, $timeout) {
            if ($.cookie('token')) {
              keazAPI.validateLogin().error(function () {
                Auth.destroyAll();
                $location.path('/login');
              }).success(function (data) {
                if ($.inArray('install',data.perms) == -1){
                  $rootScope.show_msg('Permission denied, only installer can access', false);
                  Auth.destroyAll();
                  $location.path('/login');
                }else{
                  var vehicle_kit_companies = {};
                  for (var i = 0; i < data.companies.length; i++) {
                    vehicle_kit_companies[data.companies[i]['id']] = data.companies[i];
                  }
                  $rootScope.companies_listing = data.companies;
                  data['companies'] = vehicle_kit_companies;
                  $rootScope.user_data = data;
                }
              });
            } else {
              Auth.destroyAll();
              $location.path('/login');
            }
          }]
        }
      })
      .state('base', {
        abstract: true,
        url: '',
        templateUrl: 'layout/base.html'
      })
      .state('login', {
        url: '/login',
        parent: 'base',
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl'
      })
      .state('reports', {
        url: '/reports',
        parent: 'auth',
        templateUrl: 'views/dashboard/reports.html'
      }).
      //start
      state('company', {
        url: '/company',
        parent: 'auth',
        templateUrl: 'views/dashboard/company.html',
        controller: 'CompanyCtrl'
      }).
      // TODO
      state('select', {
        url: '/select',
        parent: 'auth',
        templateUrl: 'views/dashboard/select.html',
        controller: 'SelectCtrl'
      }).
      state('new_vehicle', {
        url: '/vehicle',
        parent: 'auth',
        templateUrl: '../views/dashboard/vehicle.html',
        controller: 'DetailVehicleCtrl'
      }).
      state('assign_kit', {
        url: '/assign_kit',
        parent: 'auth',
        templateUrl: '../views/dashboard/assign_kit.html',
        controller: 'AssignKitCtrl'
      }).
      state('edit_vehicle', {
        url: '/vehicle/{id}',
        parent: 'auth',
        templateUrl: '../views/dashboard/vehicle.html',
        controller: 'DetailVehicleCtrl'
      }).
      state('vehicles', {
        url: '/vehicles',
        parent: 'auth',
        templateUrl: '../views/dashboard/vehicles.html',
        controller: 'ListVehicleCtrl'
      }).
      state('rfid', {
        url: '/rfid',
        parent: 'auth',
        templateUrl: '../views/dashboard/rfid.html',
        controller: 'RfidCtrl'
      }).
      state('test_vehicle', {
        url: '/test_vehicle/{id}',
        parent: 'auth',
        templateUrl: 'views/dashboard/test_vehicle.html',
        controller: 'TestVehicleCtrl'
      });

  });
