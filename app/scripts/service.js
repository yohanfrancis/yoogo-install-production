/**
 * Created by TranTanSang on 8/14/2015.
 */
'use strict';
$ = jQuery;
var api_url = 'https://api.yoogoshare.co.nz/v1/';
var client_version = '';
var client_app_name = 'INSTALL';
var client_device_type = 'WEBSITE';


if (!$.cookie("company")) {
  $.cookie("company", 'keaz');
}
angular.module('service', [])
  .factory('Auth', function ($rootScope) {
    return {
      destroyAll: function () {
        $.removeCookie('token');
        $.removeCookie('email');
        $.cookie("company", 'keaz');
      },
      createSession: function (data) {
        $.cookie("email", data.email);
        $.cookie("token", data.token);
      },
      getSession: function () {
        if ($.cookie("token")) {
          return {
            'token': $.cookie("token"),
            'email': $.cookie("email")
          }
        }
        return null;
      }
    }
  })
  .factory('Request', ["$http", "$location", "$rootScope", "Auth", "$timeout", function ($http, $location, $rootScope, Auth, $timeout) {
    return {
      get: function (option) {
        $rootScope.load = true;
        var validate_response = function (data) {
          if (data && data.title && data.title == 'token required' && $location.path() != '/login') {
            Auth.destroyAll();
            $timeout(function () {
              $location.path('/login');
            });
          }
          else if (data && data.title && data.description) {
            $rootScope.show_msg(data.description || 'Error', false);
          }
          $rootScope.load = false;
        };
        if (option.headers) {
          option.headers['X-Source-Host'] = $.cookie("company");
          option.headers['Token'] = $.cookie('token');
          option.headers['Version'] = client_version;
          option.headers['DeviceType'] = client_device_type;
          option.headers['AppName'] = client_app_name;
        }
        else {
          option.headers = {
            'X-Source-Host': $.cookie("company"),
            'Token': $.cookie('token'),
            'Version': client_version,
            'DeviceType': client_device_type,
            'AppName': client_app_name
          };
        }
        return $http(option).success(validate_response).error(validate_response);
      }
    }
  }])
  .factory('keazAPI', ['Request', "Auth", '$location', '$rootScope', '$timeout', '$http', function (Request, Auth, $location, $rootScope, $timeout, $http) {
    var keazAPI = {};
    keazAPI.login = function (_data) {
      _data = JSON.stringify(_data);
      return Request.get({
        method: 'POST',
        url: api_url + 'login',
        data: _data,
        headers: {
          'X-Source-Host': $.cookie("company")
        }
      });
    };
    keazAPI.validateLogin = function () {
      return Request.get({
        method: 'GET',
        url: api_url
      });
    };
    keazAPI.get = function (_url, _withoutToken) {
      var URL = api_url + _url;
      return Request.get({
        method: 'GET',
        url: URL
      });
    };
    keazAPI.post = function (_url, _data, _withoutToken) {
      var _data = JSON.stringify(_data),
        _url = api_url + _url;
      return Request.get({
        method: 'POST',
        url: _url,
        data: _data
      });
    };
    keazAPI.delete = function (_url) {
      var _url = api_url + _url;
      return Request.get({
        method: 'DELETE',
        url: _url
      });
    };
    keazAPI.put = function (_url, _data, _withoutToken) {
      var _data = JSON.stringify(_data),
        _url = api_url + _url;
      return Request.get({
        method: 'PUT',
        url: _url,
        data: _data
      });
    };
    return keazAPI;
  }]);
